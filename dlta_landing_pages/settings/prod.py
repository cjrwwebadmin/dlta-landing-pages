from . base import *

DEBUG = False

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en//ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['.cjrw.rocks',
                 'order.diamondlakesguide.com']

# Static asset configuration
STATIC_ROOT = '/home/cjrw/apps/dlta_landing_static'

# Upload file configuration
MEDIA_ROOT = '/home/cjrw/apps/dlta_landing/uploadfiles'

# CACHE CONFIG
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}

# EMAIL SETTINGS
EMAIL_HOST = 'smtp.mailgun.org'
EMAIL_HOST_USER = 'postmaster@mg.cjrw.rocks'
EMAIL_HOST_PASSWORD = 'bbd97c104d12694f58f1ecbb3f0e5184-985b58f4-950170dc'
DEFAULT_FROM_EMAIL = 'no-reply@diamondlakesguide.com'
SERVER_EMAIL = 'no-reply@diamondlakesguide.com'

# LEADS SETTTINGS
LEADS_EMAIL_RECIPIENT_LIST = ['webadmin@cjrw.com']
