from . base import *

DEBUG = True

# CACHE CONFIG
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}

#INSTALLED_APPS += (
#    'debug_toolbar',
#)

#MIDDLEWARE.append('debug_toolbar.middleware.DebugToolbarMiddleware')

#INTERNAL_IPS = ['127.0.0.1',]

# EMAIL CONFIGURATION
# EMAIL_BACKEND = 'django.core.mail.backends.dummy.EmailBackend'
EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
EMAIL_FILE_PATH = os.path.join(BASE_DIR, '..', 'emailout')


# LEADS SETTTINGS
LEADS_EMAIL_RECIPIENT_LIST = ['wade.austin@gmail.com']
