from django.conf import settings
from django.urls import include, re_path, path
from django.contrib import admin

from filebrowser.sites import site

from .views import HomePageRedirectView


app_name = "start_test"

urlpatterns = [
    # Home
    re_path(r'^$', HomePageRedirectView.as_view(), name='home'),

    # Filebrowser, DJ Admin, & Grappelli
    re_path(r'^admin/filebrowser/', site.urls),
    path('admin/', admin.site.urls),

    # Landing Pages
    path('', include('landing_pages.urls')),
]

# UPLOAD MEDIA IN DEBUG
if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        re_path(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
