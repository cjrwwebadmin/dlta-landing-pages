# Project specific views go here
from django.shortcuts import get_list_or_404
from django.views.generic.base import RedirectView

from landing_pages.models import LandingPage


# Redirect the home page to a specific landing  page or 404
class HomePageRedirectView(RedirectView):
    permanent = False

    def get_redirect_url(self, *args, **kwargs):
        default_lp_list = get_list_or_404(LandingPage, default_landing_page=True)
        return default_lp_list[0].get_absolute_url()
