# Generated by Django 2.0.4 on 2018-04-20 09:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('landing_pages', '0003_landingpage_footer_area_content'),
    ]

    operations = [
        migrations.AddField(
            model_name='landingpage',
            name='default_landing_page',
            field=models.BooleanField(db_index=True, default=False, help_text='The default landing page. Maybe used to redirect home page to a landing page. Only one should be selected or "first" on sorted is used.'),
        ),
    ]
