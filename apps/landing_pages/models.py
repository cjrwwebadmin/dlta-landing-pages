from django.conf import settings
from django.db import models

from filebrowser.fields import FileBrowseField


class LandingPage(models.Model):
    title = models.CharField(max_length=200)
    slug = models.SlugField(max_length=200, unique=True)
    content = models.TextField(blank=True)
    landing_page_tracking_id = models.CharField(max_length=200,
                                                help_text='A tracking ID that can be passed to the form to track which landing page a lead comes from')

    thank_you_content = models.TextField(blank=True, help_text='Copy for the landing pages success/thank-you page')

    footer_area_content = models.TextField(blank=True)

    header_image = FileBrowseField(max_length=200,
                                   blank=True,
                                   directory=settings.LANDING_PAGES_HEADER_IMAGE_DIR,
                                   format='image',
                                   help_text='Header image for the top of the page')

    default_landing_page = models.BooleanField(blank=True, default=False, db_index=True,
                                               help_text='The default landing page. Maybe used to redirect home page to a landing page. Only one should be selected or "first" on sorted is used.')

    def __str__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return ('landing_pages_landingpage_detail', (), {'slug': self.slug})

    @models.permalink
    def get_thank_you_page_url(self):
        """ Return the thank you / sucess page for this landing page """
        return ('landing_pages_landingpage_thankyou', (), {'slug': self.slug})

    class Meta:
        ordering = ('slug',)
