from django.apps import AppConfig


class LandingPagesConfig(AppConfig):
    name = 'landing_pages'
    verbose_name = "Landing Pages"
