from django.conf import settings
from django import template

register = template.Library()


@register.simple_tag
def get_online_pub_url():
    return settings.LANDING_PAGE_EMAIL_REDIRECT_URL
