from django.urls import path

from .views import LandingPageView, LandingPageThankYouView, EmailSignUpView

urlpatterns = [
    path('<slug:slug>/',
         LandingPageView.as_view(),
         name='landing_pages_landingpage_detail'),
    path('<slug:slug>/email-signup/',
         EmailSignUpView.as_view(),
         name='landing_pages_landingpage_emailsignup'),
    path('<slug:slug>/thanks/',
         LandingPageThankYouView.as_view(),
         name='landing_pages_landingpage_thankyou'),
]
