from django.contrib import admin
from django.conf import settings
from django import forms

from .models import LandingPage


class LandingPageAdminForm(forms.ModelForm):
    class Meta:
        widgets = {
            "content": forms.Textarea(attrs={'class': 'mceSimple', 'size': '40'}),
            "thank_you_content": forms.Textarea(attrs={'class': 'mceSimple', 'size': '40'}),
        }


@admin.register(LandingPage)
class LandingPage(admin.ModelAdmin):
    form = LandingPageAdminForm
    list_display = ('title', 'slug', 'landing_page_tracking_id', 'default_landing_page')
    list_filter = ['default_landing_page', 'landing_page_tracking_id']
    prepopulated_fields = {'slug': ('title',)}
    search_fields = ('title', 'content',)

    fieldsets = (
        (None, {
            'fields': ('title', 'slug', 'landing_page_tracking_id', 'default_landing_page'),
            'classes': ('mceEditor',),
        }),
        ('Body Content', {
            'fields': ('content', 'footer_area_content'),
            'classes': ('full-width',),
        }),
        ('Thank You Page Content', {
            'fields': ('thank_you_content',),
            'classes': ('full-width',),
        }),
        ('Images', {
            'fields': ('header_image',),
        }),
    )

    class Media:
        js = (settings.STATIC_URL + 'js/tiny_mce/tiny_mce.js',
              settings.STATIC_URL + 'js/tiny_mce_simple_init.js')
