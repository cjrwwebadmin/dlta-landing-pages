from django.conf import settings
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.views.generic import FormView, DetailView
from django.views.generic.detail import SingleObjectMixin

from leads.forms import CustomerLeadForm, EmailSignupForm
from .models import LandingPage


# A Generic View mixin or common landing page tasks
class LandingPageMixin(SingleObjectMixin):
    model = LandingPage

    # This will be passed to the view from the model als
    referral_landing_page_id = 'GENERIC_LEAD'

    def dispatch(self, request, *args, **kwargs):
        # Grab the tracking code and referral id for this form
        self.object = self.get_object()
        self.landing_page = self.object
        if self.landing_page.landing_page_tracking_id:
            self.referral_landing_page = self.landing_page.landing_page_tracking_id
        return super().dispatch(request, *args, **kwargs)


# Process the order view
class LandingPageView(LandingPageMixin, FormView):
    form_class = CustomerLeadForm
    template_name = "landing_pages/landing_page_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['email_form'] = EmailSignupForm(form_action=reverse_lazy('landing_pages_landingpage_emailsignup', kwargs={'slug': self.landing_page.slug}))
        return context

    def get_success_url(self):
        return reverse('landing_pages_landingpage_thankyou', kwargs={'slug': self.landing_page.slug})

    def form_valid(self, form):
        form.save(referral_landing_page=self.referral_landing_page,
                  referral_type="GUIDE_REQUEST")
        return super().form_valid(form)


# Handle Email Sign Ups
class EmailSignUpView(LandingPageMixin, FormView):
    model = LandingPage
    form_class = EmailSignupForm
    referral_landing_page_id = 'GENERIC_LEAD'
    referral_type = 'EMAIL'

    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect(reverse_lazy('landing_pages_landingpage_detail', kwargs={'slug': self.landing_page.slug}))

    def dispatch(self, request, *args, **kwargs):
        # Grab the tracking code and referral id for this form
        self.object = self.get_object()
        self.landing_page = self.object
        if self.landing_page.landing_page_tracking_id:
            self.referral_landing_page = self.landing_page.landing_page_tracking_id
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        form.save(referral_landing_page=self.referral_landing_page)
        return super().form_valid(form)

    def get_success_url(self):
        return settings.LANDING_PAGE_EMAIL_REDIRECT_URL


class LandingPageThankYouView(DetailView):
    model = LandingPage
    template_name = "landing_pages/landing_page_thank_you.html"
