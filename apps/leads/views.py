from django.views.generic import FormView

from .forms import CustomerLeadForm

from django.http import HttpResponse
from django.contrib.admin.views.decorators import staff_member_required
import csv
import datetime


from .models import CustomerLead


class CustomerLeadView(FormView):
    form_class = CustomerLeadForm
    referral_landing_page = 'GENERIC_LANDING_PAGE'
    referral_type = 'GENERIC'

    def form_valid(self, form):
        form.save(referral_landing_page=self.referral_landing_page,
                  referral_type=self.referral_type)
        return super().form_valid(form)


@staff_member_required
def export_leads(request):
    """ Export Leads as CSV, mark those that have been exported """
    response = HttpResponse(content_type="text/csv")
    today = datetime.date.today()

    response['Content-Disposition'] = 'attachment; filename=customer_leads_{0}.csv'.format(today)

    writer = csv.writer(response, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)

    leads = CustomerLead.objects.filter(exported_on__isnull=True)

    writer.writerow(['Email', 'First Name', 'Last Name', 'Phone',
                     'Address', 'City', 'State', 'Postal Code', 'Country',
                     'Marketing Opt-In', 'Submission Date',
                     'Referral Type', 'Referral Landing Page'])
    for l in leads:
        writer.writerow([l.email, l.first_name, l.last_name, l.phone,
                         l.address, l.city, l.state, l.postal_code, l.country,
                         l.marketing_opt_in, l.created_on.date(),
                         l.referral_type, l.referral_landing_page])

        l.exported_on = datetime.datetime.now()
        l.save()

    return response
