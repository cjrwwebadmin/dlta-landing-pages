# Generated by Django 2.0.4 on 2018-04-20 09:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('leads', '0006_auto_20180418_1608'),
    ]

    operations = [
        migrations.AddField(
            model_name='customerlead',
            name='exported_on',
            field=models.DateTimeField(db_index=True, null=True),
        ),
    ]
