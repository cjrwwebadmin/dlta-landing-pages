from django.db import models
from localflavor.us.models import USStateField


class CustomerLead(models.Model):
    first_name = models.CharField(max_length=100, blank=True)
    last_name = models.CharField(max_length=100, blank=True)
    email = models.EmailField(max_length=200, db_index=True)

    phone = models.CharField(max_length=100, blank=True)

    # Market to me field
    marketing_opt_in = models.BooleanField(blank=True, default=True)

    # For Future info
    address = models.CharField(max_length=200, blank=True)
    city = models.CharField(max_length=100, blank=True)
    state = USStateField(blank=True)
    postal_code = models.CharField(max_length=25, blank=True)
    country = models.CharField(max_length=255, blank=True)
    comments = models.TextField(blank=True)

    # Referral Info
    # Referral type (print, email, etc. General notes for this referral)
    referral_type = models.CharField(max_length=250, blank=True,
                                     help_text='Referral type (print, email, etc. General notes for this referral)')

    referral_landing_page = models.CharField(max_length=250, blank=True,
                                             help_text='The landing page this referral came from ')

    # Audit info
    created_on = models.DateTimeField(auto_now_add=True, db_index=True)
    last_modified = models.DateTimeField(auto_now=True, db_index=True)
    exported_on = models.DateTimeField(db_index=True, null=True, blank=True)

    def __str__(self):
        return "{0}".format(self.email, )

    class Meta:
        ordering = ('-created_on', 'last_name')
