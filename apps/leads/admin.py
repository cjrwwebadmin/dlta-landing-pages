from django.contrib import admin
from .models import CustomerLead

from django.urls import path

from .views import export_leads


@admin.register(CustomerLead)
class CustomerLeadAdmin(admin.ModelAdmin):
    list_display = ['email', 'first_name', 'last_name', 'referral_type', 'referral_landing_page', 'created_on', 'exported_on']
    date_heirarchy = 'created_on'
    list_filter = ['created_on', 'exported_on', 'referral_type', 'referral_landing_page']
    search_fields = ['email', 'first_name', 'last_name']

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('export', export_leads, name='leads_export')
        ]
        return urls + my_urls
