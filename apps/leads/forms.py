from django import forms
from django.conf import settings
from django.core import mail
from django.template.loader import render_to_string

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Field, Div


import logging
from smtplib import SMTPException
from .models import CustomerLead

logger = logging.getLogger('leads.forms')


class EmailForm(object):
    email_recipients = settings.LEADS_EMAIL_RECIPIENT_LIST
    subject_line = settings.LEADS_EMAIL_SUBJECT_LINE
    body_template_name = 'leads/emails/lead_email_body.txt'

    def get_context(self):
        """ Return a context dictionary for the email body template """
        return self.cleaned_data.copy()

    def send_email(self, fail_silently=False, *args, **kwargs):
        context = self.get_context()
        context.update(kwargs)
        message_body = render_to_string(self.body_template_name, context)
        try:
            message = mail.EmailMessage(
                subject=self.subject_line,
                body=message_body,
                from_email=self.cleaned_data['email'],
                to=self.email_recipients,
                headers={
                    'Reply-To': self.cleaned_data['email']
                }
            )
            message.send()
        except SMTPException:
            logger.exception("An error occured while sending the lead form.")
            return False
        else:
            return True


class CustomerLeadForm(forms.ModelForm, EmailForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields[settings.HONEYPOT_FIELD_NAME] = forms.CharField(required=False)

        # Set optional model fields, required on the form
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['address'].required = True
        self.fields['city'].required = True
        self.fields['state'].required = True
        self.fields['postal_code'].required = True
        self.fields['country'].required = True
        self.fields['country'].initial = "U.S.A."

        # Fix state widget

        self.helper = FormHelper()
        self.helper.form_id = 'lead-form_id'
        self.helper.form_class = 'lead-form'
        self.helper.form_method = 'POST'

        self.helper.layout = Layout(
            Div(
                Field('first_name'), Field('last_name'),
                css_class='name-fields'
            ),
            Div(
                Field('email'), Field('phone'),
                css_class="email-phone-fields",
            ),
            Div(
                Field('address'),
                Div(
                    Field('city'), Field('state'), Field('postal_code'), Field('country'),
                    css_class="city-state-zip-fields"
                ),
                css_class="address-fields"
            ),
            Div(
                Field(settings.HONEYPOT_FIELD_NAME),
                css_class="hp-fields"
            ),
            Field('marketing_opt_in'),
            Submit('submit-lead', 'Request Your Guide', css_class='button'),
        )

    def save(self, referral_landing_page, referral_type, commit=False, *args, **kwargs):
        # explicit get instance, make changes, and save it
        lead = super().save(commit, *args, **kwargs)
        lead.referral_landing_page = referral_landing_page
        lead.referral_type = referral_type
        lead.save()
        self.send_email(referral_type=lead.referral_type,
                        referral_landing_page=lead.referral_landing_page,
                        created_on=lead.created_on)

    class Meta:
        model = CustomerLead
        fields = [
            'first_name', 'last_name', 'email', 'phone', 'marketing_opt_in',
            'address', 'city', 'state', 'postal_code', 'country',
        ]
        labels = {
            'marketing_opt_in': 'YES, SIGN ME UP TO RECEIVE DEALS AND OFFERS!'
        }


class EmailSignupForm(forms.ModelForm, EmailForm):
    referral_type = 'EMAIL_SIGNUP'

    def __init__(self, form_action='', *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields[settings.HONEYPOT_FIELD_NAME] = forms.CharField(required=False)

        self.fields['email'].required = False

        self.helper = FormHelper()
        self.helper.form_id = 'lead-email-form_id'
        self.helper.form_class = 'lead-email-form'
        self.helper.form_method = 'POST'
        self.helper.form_action = form_action

        self.helper.layout = Layout(
            Field('email'),
            Div(
                Field(settings.HONEYPOT_FIELD_NAME),
                css_class="hp-fields"
            ),
            Submit('submit-email', 'Sign Up', css_class='button'),
        )

    def save(self, referral_landing_page, commit=False, *args, **kwargs):
        # only save the form if an email address was provided
        if self.cleaned_data['email'] and len(self.cleaned_data['email']):
            # explicit get instance, make changes, and save it
            lead = super().save(commit, *args, **kwargs)
            lead.referral_type = self.referral_type
            lead.referral_landing_page = referral_landing_page
            lead.save()

    class Meta:
        model = CustomerLead
        fields = [
            'email',
        ]
        labels = {
            'email': 'Email Address'
        }
